﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ExifTool
{
    /// <summary>
    /// Shell.xaml 的交互逻辑
    /// </summary>
    public partial class Shell : Window
    {
        public Shell()
        {
            System.Windows.Application.Current.DispatcherUnhandledException += App_OnDispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            InitializeComponent();
            this.borderTitle.MouseMove += (sender, e) =>
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    base.DragMove();
                }
            };
            string appTitle = ConfigurationManager.AppSettings["AppTitle"];
            if (!string.IsNullOrEmpty(appTitle) && !string.IsNullOrWhiteSpace(appTitle))
            {
                this.txtTitle.Text = appTitle;
            }

            this.EnterNavigateView();
        }

        private NavigateView navigateView;
        private FolderWorkView folderWorkView;
        private ImageView imageView;
        private void EnterFolderStyle()
        {
            if (folderWorkView == null)
            {
                folderWorkView = new FolderWorkView();
                this.folderWorkView.EnterNavigateView += (sender,e) => {this.EnterNavigateView(); };
            }
            this.ContendGrid.Children.Clear();
            this.ContendGrid.Children.Add(folderWorkView);
        }
        private void EnterImageStyle()
        {
            if (this.imageView == null)
            {
                imageView = new ImageView();
                this.imageView.EnterNavigateView += (sender, e) => { this.EnterNavigateView(); };
            }
            this.ContendGrid.Children.Clear();
            this.ContendGrid.Children.Add(imageView);
        }
        private void EnterNavigateView()
        {
            if (this.navigateView==null)
            {
                navigateView = new NavigateView();
                navigateView.EnterFolderView += (_sender, _e) => { this.EnterFolderStyle(); };
                navigateView.EnterImageView += (_sender, _e) => { this.EnterImageStyle(); };
            }
            this.ContendGrid.Children.Clear();
            this.ContendGrid.Children.Add(navigateView);
        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show($"将要关闭软件，是否继续？", "关闭软件提示", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                Window.GetWindow(this).Close();
            }


        }

        #region 全局错误处理

        /// <summary>
        /// UI线程抛出全局异常事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                LogUtil.LogtoText(e.Exception.ToString());
                string contend = e.Exception.Message;
                if (e.Exception.InnerException != null)
                {
                    contend = contend + "\r\n" + e.Exception.InnerException.Message;
                }
                System.Windows.MessageBox.Show(contend,"警告信息",MessageBoxButton.OK,MessageBoxImage.Warning);
                e.Handled = true;
            }
            catch (Exception ex)
            {
                LogUtil.LogtoText( "不可恢复的UI线程全局异常  " + ex);
                System.Windows.MessageBox.Show("应用程序发生不可恢复的异常，将要退出！", "警告信息", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// 非UI线程抛出全局异常事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject != null && e.ExceptionObject is Exception exception)
            {
                string contend = exception.Message;
                LogUtil.LogtoText(exception.ToString() + "非UI线程全局异常");
                System.Windows.MessageBox.Show(contend, "警告信息", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                LogUtil.LogtoText("非UI线程全局异常");
                System.Windows.MessageBox.Show("非UI线程全局异常", "警告信息", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (e.IsTerminating)
            {
                Environment.Exit(0);
            }
        }

        #endregion
    }

    
}
