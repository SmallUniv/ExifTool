﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using MessageBox = System.Windows.MessageBox;
using Path = System.IO.Path;

namespace ExifTool
{
    /// <summary>
    /// FolderWorkView.xaml 的交互逻辑
    /// </summary>
    public partial class FolderWorkView : System.Windows.Controls.UserControl
    {
        /// <summary>
        /// 请求进入导航视图
        /// </summary>
        public event EventHandler EnterNavigateView;
        private readonly string[] imgExts = new string[] { ".jpg", ".jpeg", ".png" ,"bmp"};
        public FolderWorkView()
        {
            InitializeComponent();
            exifTool = new ExifUtil();
            this.Loaded += FolderWorkView_Loaded;
        }
        private ExifUtil exifTool;
        private void FolderWorkView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Init();
        }

     
        private void SelectFolderButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "选择操作的文件夹";
            folderBrowserDialog.ShowNewFolderButton = false;
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.SourceImgFolderTextBox.Text = folderBrowserDialog.SelectedPath;
            }

        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string sourceDir = this.SourceImgFolderTextBox.Text;
            if (!Directory.Exists(sourceDir))
            {
                MessageBox.Show("请先选择要执行的文件夹", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            string[] imgs = Directory.GetFiles(sourceDir)
                .Where(i => this.imgExts.Contains(Path.GetExtension(i).ToLower())).ToArray();
            if (imgs.Length < 1)
            {
                MessageBox.Show($"该文件夹下 {sourceDir} 没有图像", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (MessageBox.Show($"将要对原始图像执行去地理坐标操作，是否继续？", "关闭软件提示", MessageBoxButton.YesNo, MessageBoxImage.Information) != MessageBoxResult.Yes)
            {
                return;
            }

            ActTaskBefore();
            
            Task.Factory.StartNew((()=>
            {
                int count = imgs.Length;
                for (int i = 0; i < count; i++)
                {
                    string img = imgs[i];
                    this.exifTool.DeleteCoord(img);

                    double percent = i / (count + 0.0);
                    this.Dispatcher.Invoke(new Action(() => { this.ProgressBar.Value = percent*100; }));
                }

                this.Dispatcher.Invoke(new Action(ActTaskLater));
            }));
        }

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            string sourceDir = this.SourceImgFolderTextBox.Text;
            if (!Directory.Exists(sourceDir))
            {
                MessageBox.Show("请先选择要执行的文件夹", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            string[] imgs = Directory.GetFiles(sourceDir)
                .Where(i => this.imgExts.Contains(Path.GetExtension(i).ToLower())).ToArray();
            if (imgs.Length < 1)
            {
                MessageBox.Show($"该文件夹下 {sourceDir} 没有图像", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "选择输出文件夹";
            folderBrowserDialog.ShowNewFolderButton = false;
            if (folderBrowserDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            string outputPath = folderBrowserDialog.SelectedPath;



            ActTaskBefore();
            Task.Factory.StartNew((() =>
            {
                int count = imgs.Length;
                for (int i = 0; i < count; i++)
                {
                    string img = imgs[i];
                    string outImg = System.IO.Path.Combine(outputPath, System.IO.Path.GetFileName(img));
                    this.exifTool.DeleteCoord(img,outImg);

                    double percent = i / (count + 0.0);
                    this.Dispatcher.Invoke(new Action(() => { this.ProgressBar.Value = percent*100; }));
                }
                this.Dispatcher.Invoke(new Action(ActTaskLater));
              
            }));
        }
    
        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.EnterNavigateView?.Invoke(this,new EventArgs());
        }

        #region private

        private void Init()
        {
            this.ProgressBar.Value = 0;
            this.ProgressBar.Visibility = Visibility.Collapsed;
            this.SourceImgFolderTextBox.Text = null;
        }
        private void ActTaskLater()
        {
            this.ProgressBar.Visibility = Visibility.Collapsed;
            this.BackButton.IsEnabled = true;
            this.SaveButton.IsEnabled = true;
            this.SaveAsButton.IsEnabled = true;
            MessageBox.Show("执行完毕", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// 执行业务之前
        /// </summary>
        private void ActTaskBefore()
        {
            this.ProgressBar.Visibility = Visibility.Visible;
            this.BackButton.IsEnabled = false;
            this.SaveButton.IsEnabled = false;
            this.SaveAsButton.IsEnabled = false;
        }


        #endregion
    }
}
