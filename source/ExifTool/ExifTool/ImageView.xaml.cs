﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;
using UserControl = System.Windows.Controls.UserControl;

namespace ExifTool
{
    /// <summary>
    /// ImageView.xaml 的交互逻辑
    /// </summary>
    public partial class ImageView : UserControl
    {
        /// <summary>
        /// 请求进入导航视图
        /// </summary>
        public event EventHandler EnterNavigateView;
        public ImageView()
        {
            InitializeComponent();
            exifTool = new ExifUtil();
            this.Loaded += ImageView_Loaded;
        }

        private ExifUtil exifTool;
        private void ImageView_Loaded(object sender, RoutedEventArgs e)
        {
           this.Init();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string[] addedImgs = this.GetAddedImages();
            if (addedImgs.Length<1)
            {
                MessageBox.Show("请先选择要执行的图像文件", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (MessageBox.Show($"将要对原始图像执行去地理坐标操作，是否继续？", "关闭软件提示", MessageBoxButton.YesNo, MessageBoxImage.Information) != MessageBoxResult.Yes)
            {
                return;
            }
            int count = addedImgs.Length;
            if (count == 1)
            {
                this.exifTool.DeleteCoord(addedImgs[0]);
                ActTaskLater();
            }
            else
            {
                ActTaskBefore();
                Task.Factory.StartNew((() =>
                {
                    for (int i = 0; i < count; i++)
                    {
                        string img = addedImgs[i];
                        this.exifTool.DeleteCoord(img);
                        double percent = i / (count + 0.0);
                        this.Dispatcher.Invoke(new Action(() => { this.SetProgessBar(percent); }));
                    }
                    this.Dispatcher.Invoke(new Action(ActTaskLater));

                }));
                
            }

        }

       

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            string[] addedImgs = this.GetAddedImages();
            if (addedImgs.Length < 1)
            {
                MessageBox.Show("请先选择要执行的图像文件", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "选择输出文件夹";
            folderBrowserDialog.ShowNewFolderButton = false;
            if (folderBrowserDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
               return;
            }
            string outputPath = folderBrowserDialog.SelectedPath;

            int count = addedImgs.Length;
            if (count == 1)
            {
                string outImg = System.IO.Path.Combine(outputPath, System.IO.Path.GetFileName(addedImgs[0]));
                this.exifTool.DeleteCoord(addedImgs[0], outImg);
                ActTaskLater();
            }
            else
            {
                ActTaskBefore();
                Task.Factory.StartNew((() =>
                {
                    for (int i = 0; i < count; i++)
                    {
                        string img = addedImgs[i];
                        string outImg = System.IO.Path.Combine(outputPath, System.IO.Path.GetFileName(img));
                        this.exifTool.DeleteCoord(img, outImg);
                        double percent = i / (count + 0.0);
                        this.Dispatcher.Invoke(new Action(() => { SetProgessBar(percent); }));
                    }
                    this.Dispatcher.Invoke(new Action(ActTaskLater));

                }));

            }
        }
     

        private void AddImgButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "图像|*.jpg;jpeg;JPG;JPEG;png;bmp|xml文件|*.xml|所有文件|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string[] selectedImgs = openFileDialog.FileNames;
                
                if (this.SourceImgListBox.HasItems)
                {
                    //如若存在添加过的图像，让用户重新选择
                    List<string> addedImgs = this.GetAddedImages().ToList();
                    foreach (string selectedImg in selectedImgs)
                    {
                        if (addedImgs.Contains(selectedImg))
                        {
                            MessageBox.Show($"选择的图像 {selectedImg} 已经添加过了。请重新添加", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                    }
                    foreach (string selectedImg in selectedImgs)
                    {
                        this.SourceImgListBox.Items.Add(selectedImg);
                    }
                }
                else
                {
                    foreach (string selectedImg in selectedImgs)
                    {
                        this.SourceImgListBox.Items.Add(selectedImg);
                    }
                }
            }
        }

        private void ClearImgButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SourceImgListBox.HasItems)
            {
                this.SourceImgListBox.Items.Clear();
            }

        }
        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.EnterNavigateView?.Invoke(this, new EventArgs());
        }

        #region private
        private void Init()
        {
            this.ProgressBar.Value = 0;
            this.ProgressBar.Visibility = Visibility.Collapsed;
            this.SourceImgListBox.Items.Clear();
        }
        /// <summary>
        /// 执行业务前
        /// </summary>
        private void ActTaskBefore()
        {
            this.ProgressBar.Visibility = Visibility.Visible;
            this.BackButton.IsEnabled = false;
            this.SaveAsButton.IsEnabled = false;
            this.SaveButton.IsEnabled = false;
        }
        /// <summary>
        /// 执行业务后
        /// </summary>
        private void ActTaskLater()
        {
            this.ProgressBar.Visibility = Visibility.Collapsed;
            this.BackButton.IsEnabled = true;
            this.SaveAsButton.IsEnabled = true;
            this.SaveButton.IsEnabled = true;
            MessageBox.Show("执行完毕", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        /// <summary>
        /// 获取已经添加的图像
        /// </summary>
        /// <returns></returns>
        private string[] GetAddedImages()
        {

            if (this.SourceImgListBox.HasItems)
            {
                List<string> selectedImgs = new List<string>();
                foreach (object item in this.SourceImgListBox.Items)
                {
                    selectedImgs.Add(item as string);
                }
                return selectedImgs.ToArray();
            }
            else
            {
                return new string[0];
            }

        }
        /// <summary>
        /// 设置进度条进度
        /// </summary>
        /// <param name="percent">取值范围为0-1之间的小数</param>
        private void SetProgessBar(double percent)
        {
            this.ProgressBar.Value = percent * 100;
        }
        #endregion
    }
}
