﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Documents;

namespace ExifTool
{
    public class LogUtil
    {
        private static string logDir = AppDomain.CurrentDomain.BaseDirectory + "logs";
        private static string logFile;

        static LogUtil()
        {
            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }
            
        }
        public static void LogtoText(string txt)
        {
            logFile = Path.Combine(logDir, $"{DateTime.Now.ToString("yyyyMMdd")}.log");
            File.AppendAllText(logFile, DateTime.Now.ToString(CultureInfo.InvariantCulture) + " " + txt+"\r\n");
        }
    }
}
