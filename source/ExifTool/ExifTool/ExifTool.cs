﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace ExifTool
{
    /*PropertyItem 中对应属性
     * ID	Property tag
       0x0000	PropertyTagGpsVer
       0x0001	PropertyTagGpsLatitudeRef
       0x0002	PropertyTagGpsLatitude
       0x0003	PropertyTagGpsLongitudeRef
       0x0004	PropertyTagGpsLongitude
       0x0005	PropertyTagGpsAltitudeRef
       0x0006	PropertyTagGpsAltitude
     */

    /// <summary>
    /// 删除图像的经纬度信息
    /// </summary>
    public class ExifUtil
    {
        /// <summary>
        /// 删除图像的经纬度信息，覆盖原图像
        /// </summary>
        /// <param name="IN_File">文件路径</param>
        public void DeleteCoord(string IN_File)
        {
            using (Stream ms = new MemoryStream(File.ReadAllBytes(IN_File)))
            {
                using (Image image = Image.FromStream(ms))
                {
                    DeleteCoordInfo(image);
                    image.Save(IN_File);
                }
            }
        }

        
        /// <summary>
        /// 删除图像的经纬度信息,并另存为
        /// </summary>
        /// <param name="IN_File">文件路径</param>
        public void DeleteCoord(string IN_File, string IN_Save)
        {
            using (Stream ms = new MemoryStream(File.ReadAllBytes(IN_File)))
            {
                using (Image image = Image.FromStream(ms))
                {
                    DeleteCoordInfo(image);
                    image.Save(IN_Save);
                }
            }
        }
        /// <summary>
        /// 删除图像的经纬度信息
        /// </summary>
        /// <param name="image"></param>
        public static void DeleteCoordInfo(Image image)
        {
            int[] ids = new[] { 0x0000, 0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0006 };
            foreach (int id in ids)
            {
                if (image.PropertyIdList.Contains(id))
                {
                    image.RemovePropertyItem(id);
                }
            }
        }
    }
}
