﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExifTool
{
    /// <summary>
    /// NavigateView.xaml 的交互逻辑
    /// </summary>
    public partial class NavigateView : UserControl
    {
        /// <summary>
        /// 请求进入文件夹风格视图
        /// </summary>
        public event EventHandler EnterFolderView;
        /// <summary>
        /// 请求进入文件夹风格视图
        /// </summary>
        public event EventHandler EnterImageView;
        public NavigateView()
        {
            InitializeComponent();
        }

        private void FolderStyleButton_OnClick(object sender, RoutedEventArgs e)
        {
            EnterFolderView?.Invoke(this,e);
        }

        private void FileStyleButton_OnClick(object sender, RoutedEventArgs e)
        {
            EnterImageView?.Invoke(this, e);
        }
    }
}
